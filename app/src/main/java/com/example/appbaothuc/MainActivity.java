package com.example.appbaothuc;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    private ImageButton buttonAlarm;
    private ImageButton buttonSetting;
    private PendingAlarmFragment pendingAlarmFragment = new PendingAlarmFragment();
    private SettingFragment settingFragment = new SettingFragment();
    private FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonAlarm = findViewById(R.id.button_alarm);
        buttonSetting = findViewById(R.id.button_setting);
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_container, pendingAlarmFragment).commit();
    }
    public void loadPendingAlarmFragment(View view){
        fragmentManager.beginTransaction().remove(settingFragment).commit();
    }
    public void loadSettingFragment(View view){
        fragmentManager.beginTransaction().add(R.id.fragment_container, settingFragment).addToBackStack("tag").commit();
    }
}