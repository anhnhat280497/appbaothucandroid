package com.example.appbaothuc;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

public class PendingAlarmFragment extends Fragment {
    private ImageButton buttonAddAlarm;
    private FragmentManager fragmentManager;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pending_alarm, container, false);
        buttonAddAlarm = view.findViewById(R.id.button_add_alarm);
        buttonAddAlarm.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                addAlarm(v);
            }
        });
        fragmentManager = getFragmentManager();
        return view;
    }
    public void addAlarm(View view){
        AlarmFragment alarmFragment = new AlarmFragment();
        fragmentManager.beginTransaction().add(R.id.alarm_container, alarmFragment).commit();
    }
}